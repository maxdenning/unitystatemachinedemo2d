using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Understated;

namespace UnderstatedRuntimeTests
{
    public class IntegrationTest_SingleClientController
    {
        private TestSchema schema = ScriptableObject.CreateInstance<TestSchema>();

        [Test]
        public void TestActions()
        {
            // initialise client and controller
            SingleClientController<TestStates, TestEvents, TestData> controller = new SingleClientController<TestStates, TestEvents, TestData>();
            SingleClientController<TestStates, TestEvents, TestData>.Client client = new SingleClientController<TestStates, TestEvents, TestData>.Client();

            TestSchema.Controller_SetValues(controller.actions);
            TestSchema.Controller_SetStateValues(controller.actions, TestStates.One, TestStates.Two);
            TestSchema.Controller_SetStateValues(controller.actions, TestStates.Two, TestStates.Three);
            TestSchema.Controller_SetStateValues(controller.actions, TestStates.Three, TestStates.Four);
            TestSchema.Controller_SetStateValues(controller.actions, TestStates.Four, TestStates.Five);
            TestSchema.Controller_SetStateValues(controller.actions, TestStates.Five, TestStates.One);

            controller.SetClient(client, TestStates.One);
            controller.ProcessStateChanges(schema);

            //
            TestActionsHelper(schema, controller, client, TestStates.One, TestStates.Two);
            TestActionsHelper(schema, controller, client, TestStates.Two, TestStates.Three);
            TestActionsHelper(schema, controller, client, TestStates.Three, TestStates.Four);
            TestActionsHelper(schema, controller, client, TestStates.Four, TestStates.Five);
            TestActionsHelper(schema, controller, client, TestStates.Five, TestStates.One);
        }

        [Test]
        public void TestTransitions()
        {
            // initialise client and controller
            SingleClientController<TestStates, TestEvents, TestData> controller = new SingleClientController<TestStates, TestEvents, TestData>();
            SingleClientController<TestStates, TestEvents, TestData>.Client client = new SingleClientController<TestStates, TestEvents, TestData>.Client();
            controller.SetClient(client, TestStates.One);

            // enter initial state
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == TestStates.One, $"Failed to enter inital state {TestStates.One}");

            // polled triggers
            controller.PollTransitions(schema);
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == TestStates.Two, $"Failed to transition from {TestStates.One} to {TestStates.Two} using polled triggers");

            // polled selectors
            controller.PollTransitions(schema);
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == TestStates.Three, $"Failed to transition from {TestStates.Two} to {TestStates.Three} using polled selectors");

            // event trigger
            controller.SendEvent(schema, TestEvents.ChangeStateToFour, TestStates.Four);
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == TestStates.Four, $"Failed to transition from {TestStates.Three} to {TestStates.Four} using event triggers");

            // event selector
            controller.SendEvent(schema, TestEvents.ChangeStateToFive, TestStates.Five);
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == TestStates.Five, $"Failed to transition from {TestStates.Four} to {TestStates.Five} using event selectors");
        }

        private static void TestActionsHelper(
            TestSchema schema,
            SingleClientController<TestStates, TestEvents, TestData> controller,
            SingleClientController<TestStates, TestEvents, TestData>.Client client,
            TestStates fromState,
            TestStates toState)
        {
            //
            int fromValue = (int)fromState;
            int toValue = (int)toState;

            // transition to state
            Assert.IsTrue(client.state == fromState);
            client.RequestTransition(toState);
            controller.ProcessStateChanges(schema);
            Assert.IsTrue(client.state == toState);

            //
            Assert.IsTrue(client.data.schemaEnter == toValue, $"schemaEnter: {client.data.schemaEnter} =/= {toValue}");
            Assert.IsTrue(client.data.schemaEnterState == toValue, $"schemaEnterState: {client.data.schemaEnterState} =/= {toValue}");
            Assert.IsTrue(client.data.controllerEnter == toValue, $"controllerEnter: {client.data.controllerEnter} =/= {toValue}");
            Assert.IsTrue(client.data.controllerEnterState == toValue, $"controllerEnterState: {client.data.controllerEnterState} =/= {toValue}");

            //
            Assert.IsTrue(client.data.schemaExit == fromValue, $"schemaExit: {client.data.schemaExit} =/= {fromValue}");
            Assert.IsTrue(client.data.schemaExitState == fromValue, $"schemaExitState: {client.data.schemaExitState} =/= {fromValue}");
            Assert.IsTrue(client.data.controllerExit == fromValue, $"controllerExit: {client.data.controllerExit} =/= {fromValue}");
            Assert.IsTrue(client.data.controllerExitState == fromValue, $"controllerExitState: {client.data.controllerExitState} =/= {fromValue}");

            //
            Assert.IsTrue(client.data.schemaTransition == fromValue, $"schemaTransition: {client.data.schemaTransition} =/= {fromValue}");
            Assert.IsTrue(client.data.schemaTransitionState == fromValue, $"schemaTransitionState: {client.data.schemaTransitionState} =/= {fromValue}");
            Assert.IsTrue(client.data.controllerTransition == fromValue, $"controllerTransition: {client.data.controllerTransition} =/= {fromValue}");
            Assert.IsTrue(client.data.controllerTransitionState == fromValue, $"controllerTransitionState: {client.data.controllerTransitionState} =/= {fromValue}");

            //
            controller.Update(schema);
            Assert.IsTrue(client.data.schemaUpdateState == toValue, $"schemaUpdateState: {client.data.schemaUpdateState} =/= {toValue}");
            Assert.IsTrue(client.data.controllerUpdateState == toValue, $"controllerUpdateState: {client.data.controllerUpdateState} =/= {toValue}");

            //
            controller.LateUpdate(schema);
            Assert.IsTrue(client.data.schemaLateUpdateState == toValue, $"schemaLateUpdateState: {client.data.schemaLateUpdateState} =/= {toValue}");
            Assert.IsTrue(client.data.controllerLateUpdateState == toValue, $"controllerLateUpdateState: {client.data.controllerLateUpdateState} =/= {toValue}");

            //
            controller.FixedUpdate(schema);
            Assert.IsTrue(client.data.schemaLateUpdateState == toValue, $"schemaLateUpdateState: {client.data.schemaLateUpdateState} =/= {toValue}");
            Assert.IsTrue(client.data.controllerLateUpdateState == toValue, $"controllerLateUpdateState: {client.data.controllerLateUpdateState} =/= {toValue}");

            //
            controller.SendEvent(schema, TestEvents.SetValue, toValue);
            Assert.IsTrue(client.data.schemaEvent == toValue, $"schemaEvent: {client.data.schemaEvent} =/= {toValue}");
            Assert.IsTrue(client.data.controllerEvent == toValue, $"controllerEvent: {client.data.controllerEvent} =/= {toValue}");
            Assert.IsTrue(client.data.schemaEventState == toValue, $"schemaEventState: {client.data.schemaEventState} =/= {toValue}");
            Assert.IsTrue(client.data.controllerEventState == toValue, $"controllerEventState: {client.data.controllerEventState} =/= {toValue}");

            // reset values
            client.data.schemaEnter = 0;
            client.data.schemaExit = 0;
            client.data.schemaTransition = 0;
            client.data.schemaEvent = 0;
            client.data.schemaEnterState = 0;
            client.data.schemaExitState = 0;
            client.data.schemaTransitionState = 0;
            client.data.schemaEventState = 0;
            client.data.schemaUpdateState = 0;
            client.data.schemaFixedUpdateState = 0;
            client.data.schemaLateUpdateState = 0;
            client.data.controllerEnter = 0;
            client.data.controllerExit = 0;
            client.data.controllerTransition = 0;
            client.data.controllerEvent = 0;
            client.data.controllerEnterState = 0;
            client.data.controllerExitState = 0;
            client.data.controllerTransitionState = 0;
            client.data.controllerEventState = 0;
            client.data.controllerUpdateState = 0;
            client.data.controllerFixedUpdateState = 0;
            client.data.controllerLateUpdateState = 0;
        }
    }
}
