using System;
using UnityEngine;

namespace Understated
{
    // this is what happens when you don't have Spans
    internal class SliceableList<T>
    {
        public int Capacity => array.Length;
        public int Count { get; private set; } = 0;
        private T[] array  = null;

        public T this[int index]
        {
            get => array[index];
            set => array[index] = value;
        }

        public SliceableList(int capacity = 0)
        {
            array = new T[capacity];
        }

        // append
        public void Add(in T value)
        {
            if (Count >= Capacity)
            {
                SetCapacity((int)(Capacity * 1.25f) + 1);
            }

            array[Count] = value;
            Count++;
        }

        // append
        public void AddRange(in ArraySegment<T> slice)
        {
            if (Count + slice.Count >= Capacity)
            {
                SetCapacity(Count + slice.Count);
            }

            Array.Copy(slice.Array, slice.Offset, array, Count, slice.Count);
            Count += slice.Count;
        }

        // assume parameters are safe and within range
        public void RemoveRange(int start, int count)
        {
            // copy over the range to be removed
            Array.Copy(array, start + count, array, start, Count - (start + count));

            // write over duplicate end values
            Array.Clear(array, Count - count, count);

            // reset count
            Count -= count;
        }

        //
        public void UnstableRemoveAt(int index)
        {
            Count--;
            array[index] = array[Count];
            array[Count] = default(T);
        }

        // leaves garbage
        public void UnsafeRemoveAt(int index)
        {
            Count--;
            array[index] = array[Count];
        }

        // resets count
        public void Clear()
        {
            Array.Clear(array, 0, Count);
            Count = 0;
        }

        // does not reset count
        public void ClearRange(int start, int count)
        {
            Array.Clear(array, start, count);
        }

        // assume parameters are safe and within range
        public void Swap(int indexA, int indexB)
        {
            (array[indexA], array[indexB]) = (array[indexB], array[indexA]);
        }

        // short lifetime, unsafe
        public ArraySegment<T> Slice(int start, int count)
        {
            return new ArraySegment<T>(array, start, count);
        }

        // short lifetime, unsafe
        public ArraySegment<T> AsSlice()
        {
            return new ArraySegment<T>(array, 0, Count);
        }

        // only increases
        public bool SetCapacity(int size)
        {
            if (size <= Capacity)
            {
                return false;
            }

            T[] destination = new T[size];
            Array.Copy(array, 0, destination, 0, Count);
            Array.Clear(array, 0, array.Length);
            array = destination;
            return true;
        }

        // only increases
        public bool SetCount(int size)
        {
            if (size <= Count)
            {
                return false;
            }

            SetCapacity(size);
            Count = size;
            return true;
        }
    }
}
