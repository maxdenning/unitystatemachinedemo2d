using System;
using UnityEngine;

namespace Understated
{
    /// <summary>
    /// Describes a finite state machine schema in terms of events. Contains state actions,
    /// transition triggers, and transition side effects. Intended to be content agnostic and
    /// stateless, so it must be combined with a controller to create a state machine.
    /// </summary>
    /// <typeparam name="TStateKey">State label type.</typeparam>
    /// <typeparam name="TEventKey">Event label type.</typeparam>
    /// <typeparam name="TData">Data parameter type passed to all events.</typeparam>
    [Serializable]
    public class Schema<TStateKey, TEventKey, TData> : ScriptableObject
        where TStateKey : struct
        where TEventKey : struct
        where TData : class
    {
        /// <summary>
        /// Collection of events used to transform data depending on its state.
        /// </summary>
        public ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> actions = new ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>>();

        /// <summary>
        /// Collection of triggers and selectors used to indicate when data is fit to transition
        /// to another state.
        /// </summary>
        public Transitions<TStateKey, TEventKey, TData> transitions = new Transitions<TStateKey, TEventKey, TData>();

        // helper to simplify generics, e.g. public class MyStateActions : MySchema.IActionAdder {}
        public interface IActionAdder : IActionAdder<TStateKey, TEventKey, ClientCollection<TData>> {}
        public interface IActionRemover : IActionRemover<TStateKey, TEventKey, ClientCollection<TData>> {}
    }
}
