using System;
using System.Collections;
using System.Collections.Generic;

namespace Understated
{
    //
    public delegate bool PolledTrigger<TParam>(TParam data);

    //
    public delegate bool PolledSelector<TParam, TStateKey>(TParam data, ref TStateKey nextState);

    //
    public delegate bool EventTrigger<TParam, TEventContext>(TParam data, in TEventContext context);
    
    //
    public delegate bool EventSelector<TParam, TEventContext, TStateKey>(TParam data, in TEventContext context, ref TStateKey nextState);

    //
    internal class EventTriggerWrapper<TParam> : CachedGenericDelegateWrapper
    {
        public bool Test<TEventContext>(TParam data, in TEventContext context)
        {
            if (wrappedDelegate is EventTrigger<TParam, TEventContext>)
            {
                Delegate[] invocationList = GetInvocationList();
                for (int i = 0; i < Count; i++)
                {
                    if ((invocationList[i] as EventTrigger<TParam, TEventContext>).Invoke(data, context))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                // enforce one TEventContext type per TEventKey
                throw new ArgumentException("TEventContext does not match the type associated with existing delegates for this event", "TEventContext");
            }
        }
    }

    //
    internal class EventSelectorWrapper<TParam, TStateKey> : CachedGenericDelegateWrapper
    {
        public bool Test<TEventContext>(TParam data, in TEventContext context, ref TStateKey nextState)
        {
            if (wrappedDelegate is EventSelector<TParam, TEventContext, TStateKey>)
            {
                Delegate[] invocationList = GetInvocationList();
                for (int i = 0; i < Count; i++)
                {
                    if ((invocationList[i] as EventSelector<TParam, TEventContext, TStateKey>).Invoke(data, context, ref nextState))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                // enforce one TEventContext type per TEventKey
                throw new ArgumentException("TEventContext does not match the type associated with existing delegates for this event", "TEventContext");
            }
        }
    }

    //
    internal class PolledTriggerWrapper<TParam> : CachedDelegateWrapper<PolledTrigger<TParam>>
    {
        public bool Test(TParam data)
        {
            PolledTrigger<TParam>[] invocationList = GetInvocationList();
            for (int i = 0; i < Count; i++)
            {
                if (invocationList[i].Invoke(data))
                {
                    return true;
                }
            }
            return false;
        }
    }

    //
    internal class PolledSelectorWrapper<TParam, TStateKey> : CachedDelegateWrapper<PolledSelector<TParam, TStateKey>>
    {
        public bool Test(TParam data, ref TStateKey nextState)
        {
            PolledSelector<TParam, TStateKey>[] invocationList = GetInvocationList();
            for (int i = 0; i < Count; i++)
            {
                if (invocationList[i].Invoke(data, ref nextState))
                {
                    return true;
                }
            }
            return false;
        }
    }

    //
    internal class TimeoutSelectorWrapper<TStateKey, TParam> : ICollection
    {
        private List<TimeoutSelector<TStateKey, TParam>> timeoutSelectors = new List<TimeoutSelector<TStateKey, TParam>>(0);

        public TimeoutSelector<TStateKey, TParam> Add(TimeoutSelector<TStateKey, TParam> timeoutSelector)
        {
            timeoutSelectors.Add(timeoutSelector);
            return timeoutSelector;
        }

        public bool Remove(TimeoutSelector<TStateKey, TParam> timeoutSelector)
        {
            return timeoutSelectors.Remove(timeoutSelector);
        }

        public void RemoveAll()
        {
            timeoutSelectors.Clear();
        }

        public void Start<TClient>(TimeoutManager<TClient, TStateKey, TParam> timeoutRunner, double timeNow, TClient client, TParam data, ref List<int> ids)
        {
            for (int i = 0; i < timeoutSelectors.Count; i++)
            {
                ids.Add(timeoutRunner.Add(timeoutSelectors[i], client, data, timeNow));
            }
        }

        // ICollection interface
        public int Count => timeoutSelectors.Count;

        public bool IsSynchronized => throw new NotImplementedException();

        public object SyncRoot => throw new NotImplementedException();

        public void CopyTo(Array array, int index) => throw new NotImplementedException();

        public IEnumerator GetEnumerator() => throw new NotImplementedException();
    }
}
