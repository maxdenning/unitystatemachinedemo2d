using UnityEngine;

namespace Understated
{
    /*
    */
    public interface IController<TStateKey, TEventKey, TData>
        where TStateKey : struct
        where TEventKey : struct
        where TData : class
    {
        //
        void StartCoroutines(MonoBehaviour runner);
        void EndCoroutines(MonoBehaviour runner);

        //
        void ProcessStateChanges(Schema<TStateKey, TEventKey, TData> schema);
        void PollTransitions(Schema<TStateKey, TEventKey, TData> schema);

        //
        void Update(Schema<TStateKey, TEventKey, TData> schema);
        void FixedUpdate(Schema<TStateKey, TEventKey, TData> schema);
        void LateUpdate(Schema<TStateKey, TEventKey, TData> schema);

        //
        void SendEvent<TEventContext>(Schema<TStateKey, TEventKey, TData> schema, TEventKey eventKey, in TEventContext eventContext);
    }

    /*
    */
    public interface IClient<TStateKey, TData>
        where TStateKey : struct
        where TData : class
    {
        TData data { get; }
        void RequestTransition(TStateKey to);
    }
}
