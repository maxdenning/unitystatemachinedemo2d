using System;
using UnityEngine;

namespace Understated.Components
{
    /*
    */
    [Serializable]
    public class SingleClientController<TStateKey, TEventKey, TData> : ControllerComponent<TStateKey, TEventKey, TData>, IClient<TStateKey, TData>
        where TStateKey : struct
        where TEventKey : struct
        where TData : class, new()
    {
        //
        [Serializable]
        public class Client : Understated.SingleClientController<TStateKey, TEventKey, TData>.Client
        {
        }

        // expose wrapped client interface
        public TData data { get => _client.data; }
        public void RequestTransition(TStateKey to) => _client.RequestTransition(to);

        // expose client
        public Client client { get => _client; }

        // expose wrapped controller actions
        public ActionDispatcher<TStateKey, TEventKey, TData> actions => _controller.actions;

        //
        protected override IController<TStateKey, TEventKey, TData> controller => _controller;

        //
        private Understated.SingleClientController<TStateKey, TEventKey, TData> _controller = new Understated.SingleClientController<TStateKey, TEventKey, TData>();

        // hidden to force using SetClient but allows editor serialisation
        [SerializeField]
        private Client _client = null;

        public void SetClient(Client client)
        {
            _client = client;
            _controller.SetClient(client);
        }

        public void SetClient(Client client, TStateKey initialState)
        {
            _client = client;
            _controller.SetClient(client, initialState);
        }

        protected override void Awake()
        {
            _controller.StartCoroutines(this);
            _controller.SetClient(_client);
        }
    }
}
