using System;
using System.Collections.Generic;
using UnityEngine;

namespace Understated.Components
{
    /*
    */
    [Serializable]
    public class MultiClientController<TStateKey, TEventKey, TData> : ControllerComponent<TStateKey, TEventKey, TData>
        where TStateKey : struct
        where TEventKey : struct
        where TData : class, new()
    {
        //
        [Serializable]
        public class Client : MonoBehaviour, IClient<TStateKey, TData>
        {
            //
            [SerializeField]
            public TStateKey initialState = default(TStateKey);

            //
            [SerializeField]
            public Understated.MultiClientController<TStateKey, TEventKey, TData>.Client client = new Understated.MultiClientController<TStateKey, TEventKey, TData>.Client();

            // expose wrapped properties and methods
            public TData data { get => client.data; }
            public void RequestTransition(TStateKey to) => client.RequestTransition(to);
        }

        // expose wrapped controller actions
        public ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> actions => _controller.actions;

        //
        protected override IController<TStateKey, TEventKey, TData> controller => _controller;

        // add clients from the inspector
        [SerializeField]
        private List<Client> serialisedClients = new List<Client>(0);

        //
        private Understated.MultiClientController<TStateKey, TEventKey, TData> _controller = new Understated.MultiClientController<TStateKey, TEventKey, TData>();

        //
        public void AddClient(Client client) => _controller.AddClient(client.client, client.initialState);
        public bool RemoveClient(Client client) => _controller.RemoveClient(client.client);

        //
        public void AddClient(Understated.MultiClientController<TStateKey, TEventKey, TData>.Client client, TStateKey initialState) => _controller.AddClient(client, initialState);
        public void AddClients<TCollection>(TCollection clients, TStateKey initialState) where TCollection : ICollection<Understated.MultiClientController<TStateKey, TEventKey, TData>.Client> => _controller.AddClients(clients, initialState);
        public bool RemoveClient(Understated.MultiClientController<TStateKey, TEventKey, TData>.Client client) => _controller.RemoveClient(client);

        //
        public int Count(TStateKey state) => _controller.Count(state);
        public ClientCollection<TData> GetClientData(TStateKey state) => _controller.GetClientData(state);
        public void Apply(TStateKey state, Action<ClientCollection<TData>> action) => _controller.Apply(state, action);
        public void Apply(TStateKey state, Action<TData> action) => _controller.Apply(state, action);

        //
        protected override void Awake()
        {
            _controller.StartCoroutines(this);

            foreach (var client in serialisedClients)
            {
                _controller.AddClient(client.client, client.initialState);
            }
            serialisedClients = null;
        }
    }
}
