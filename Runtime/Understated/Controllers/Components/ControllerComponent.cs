using System;
using UnityEngine;

namespace Understated
{
    [Serializable]
    public struct ControllerComponentOptions
    {
        public enum ProcessPhase
        {
            Update = 0, FixedUpdate, LateUpdate,
        }

        public enum IntervalUnits
        {
            Frames = 0, //TODO: add more options, e.g. Seconds, Calls
        }

        [Header("State Change Options")]
        [Tooltip("When in the monobehaviour lifecycle to synchronise client state changes.")]
        public ProcessPhase syncPhase;

        [Tooltip("Units to use when interpreting Sync Interval.")]
        public IntervalUnits syncIntervalUnits;

        [Tooltip("Number of Sync Interval Units to wait between processing client state changes.")]
        public int syncInterval;

        [Header("Transition Polling Options")]
        [Tooltip("When in the monobehaviour lifecycle to poll transitions.")]
        public ProcessPhase pollPhase;

        [Tooltip("Units to use when interpreting Poll Interval.")]
        public IntervalUnits pollIntervalUnits;

        [Tooltip("Number of Poll Interval Units to wait between polling transitions.")]
        public int pollInterval;
    }

    /*
    */
    public abstract class ControllerComponent<TStateKey, TEventKey, TData> : MonoBehaviour
        where TStateKey : struct
        where TEventKey : struct
        where TData : class, new()
    {
        //
        [SerializeField]
        [Tooltip("The state machine schema to apply to clients of this controller.")]
        public Understated.Schema<TStateKey, TEventKey, TData> schema = null;
        
        //
        protected abstract IController<TStateKey, TEventKey, TData> controller { get; }

        //
        [SerializeField]
        [Tooltip("Options defining when client states changes are processed.")]
        private ControllerComponentOptions controllerOptions;

        //
        private int framesSinceLastSync = 0;
        private int framesSinceLastPoll = 0;

        //
        public void SendEvent<TEventContext>(TEventKey eventKey, in TEventContext eventContext)
        {
            controller.SendEvent(schema, eventKey, eventContext);
        }

        // implement MonoBehaviour methods
        protected virtual void Awake()
        {
            controller.StartCoroutines(this);
        }

        protected virtual void Update()
        {
            controller.Update(schema);

            framesSinceLastSync++;
            framesSinceLastPoll++;

            PollTransitions(ControllerComponentOptions.ProcessPhase.Update);
            ProcessStateChanges(ControllerComponentOptions.ProcessPhase.Update);
        }

        protected virtual void FixedUpdate()
        {
            controller.FixedUpdate(schema);
            PollTransitions(ControllerComponentOptions.ProcessPhase.Update);
            ProcessStateChanges(ControllerComponentOptions.ProcessPhase.FixedUpdate);
        }

        protected virtual void LateUpdate()
        {
            controller.LateUpdate(schema);
            PollTransitions(ControllerComponentOptions.ProcessPhase.Update);
            ProcessStateChanges(ControllerComponentOptions.ProcessPhase.LateUpdate);
        }

        private void PollTransitions(ControllerComponentOptions.ProcessPhase phase)
        {
            //TODO: assumes options.pollIntervalUnits is Frames
            if (phase == controllerOptions.pollPhase && framesSinceLastPoll > controllerOptions.pollInterval)
            {
                framesSinceLastPoll = 0;
                controller.PollTransitions(schema);
            }
        }

        private void ProcessStateChanges(ControllerComponentOptions.ProcessPhase phase)
        {
            //TODO: assumes options.syncIntervalUnits is Frames
            if (phase == controllerOptions.syncPhase && framesSinceLastSync > controllerOptions.syncInterval)
            {
                framesSinceLastSync = 0;
                controller.ProcessStateChanges(schema);
            }
        }
    }
}
