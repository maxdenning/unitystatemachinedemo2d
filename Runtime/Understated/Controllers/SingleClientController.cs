using System;
using System.Collections.Generic;
using UnityEngine;

namespace Understated
{
    /*
    */
    public class SingleClientController<TStateKey, TEventKey, TData> : IController<TStateKey, TEventKey, TData>
        where TStateKey : struct
        where TEventKey : struct
        where TData : class, new()
    {
        /*
        */
        [Serializable]
        public class Client : IClient<TStateKey, TData>
        {
            [SerializeField]
            public TStateKey state;

            [SerializeField]
            private TData clientData = new TData();

            internal List<int> timeouts = new List<int>(0);

            internal TStateKey pendingState = default(TStateKey);

            internal bool isStateChangePending = false;

            // IClient methods
            public TData data => clientData;

            // IClient methods
            public void RequestTransition(TStateKey to)
            {
                isStateChangePending = true;
                pendingState = to;
            }
        }


        // actions applied to clients controlled by this controller
        public ActionDispatcher<TStateKey, TEventKey, TData> actions { get; private set; } = new ActionDispatcher<TStateKey, TEventKey, TData>();

        // the client controlled by this controller
        private Client client = null;

        // wrapper for client.data, simplifies passing as ClientCollection to side effects and actions
        private TData[] data = new TData[1] { null };

        // initialised in StartCoroutines()
        private TimeoutManager<Client, TStateKey, TData> timeoutRunner = null;

        //
        private bool isClientStateInitialised = true;


        /*
        */
        public void SetClient(Client client)
        {
            SetClient(client, client.state);
        }

        /*
        */
        public void SetClient(Client client, TStateKey initialState)
        {
            this.client = client;
            data[0] = this.client.data;

            // mark the client as waiting for a state change, so the initial state will be entered
            isClientStateInitialised = false;
            this.client.RequestTransition(initialState);
        }

        /*
        */
        public void StartCoroutines(MonoBehaviour runner)
        {
            timeoutRunner = new TimeoutManager<Client, TStateKey, TData>(runner, OnTimeoutComplete);
        }

        /*
        */
        public void EndCoroutines(MonoBehaviour runner)
        {
            //
            timeoutRunner.Stop();
            runner.StopCoroutine(timeoutRunner.coroutine);
            //TODO: timeoutRunner = null;
        }

        /*
        */
        public void Update(Schema<TStateKey, TEventKey, TData> schema)
        {
            schema.actions._update?.TryGetValueOrDefault(client.state)?.Invoke(data, client.state);
            this.actions._update?.TryGetValueOrDefault(client.state)?.Invoke(data[0], client.state);
        }

        /*
        */
        public void FixedUpdate(Schema<TStateKey, TEventKey, TData> schema)
        {
            schema.actions._fixedUpdate?.TryGetValueOrDefault(client.state)?.Invoke(data, client.state);
            this.actions._fixedUpdate?.TryGetValueOrDefault(client.state)?.Invoke(data[0], client.state);
        }

        /*
        */
        public void LateUpdate(Schema<TStateKey, TEventKey, TData> schema)
        {
            schema.actions._lateUpdate?.TryGetValueOrDefault(client.state)?.Invoke(data, client.state);
            this.actions._lateUpdate?.TryGetValueOrDefault(client.state)?.Invoke(data[0], client.state);
        }

        /*
        */
        public void SendEvent<TEventContext>(Schema<TStateKey, TEventKey, TData> schema, TEventKey eventKey, in TEventContext eventContext)
        {
            //
            schema.actions._events?.TryGetValueOrDefault(eventKey)?.Invoke(data, client.state, eventContext);
            schema.actions._eventsState?.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(client.state)?.Invoke(data, client.state, eventContext);
            this.actions._events?.TryGetValueOrDefault(eventKey)?.Invoke(data[0], client.state, eventContext);
            this.actions._eventsState?.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(client.state)?.Invoke(data[0], client.state, eventContext);

            //
            if (!client.isStateChangePending && schema.transitions.HasEventTransitions())
            {
                client.isStateChangePending = TestEventTransitions(schema.transitions.GetEventTransitions(eventKey, client.state), client.data, eventContext, ref client.pendingState);
            }
        }

        /*
        */
        public void PollTransitions(Schema<TStateKey, TEventKey, TData> schema)
        {
            if (!client.isStateChangePending && schema.transitions.HasPolledTransitions())
            {
                client.isStateChangePending = TestPolledTransitions(schema.transitions.GetPolledTransitions(client.state), client.data, ref client.pendingState);
            }
        }

        /*
        */
        public void ProcessStateChanges(Schema<TStateKey, TEventKey, TData> schema)
        {
            if (!isClientStateInitialised)
            {
                isClientStateInitialised = true;
                EnterState(client.pendingState, client, data, schema.actions, this.actions, schema.transitions.GetTimeoutTransitions(client.pendingState), timeoutRunner);
            }
            else if (client.isStateChangePending)
            {
                ExitState(client.state, client, data, schema.actions, this.actions);
                TransitionState(client.state, client.pendingState, client, data, schema.actions, this.actions);
                EnterState(client.pendingState, client, data, schema.actions, this.actions, schema.transitions.GetTimeoutTransitions(client.pendingState), timeoutRunner);
            }
        }

        private static void ExitState(
            TStateKey state,
            Client client,
            TData[] data,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, TData> controllerActions)
        {
            schemaActions._exit?.Invoke(data, state);
            schemaActions._exitState?.TryGetValueOrDefault(state)?.Invoke(data, state);
            controllerActions._exit?.Invoke(data[0], state);
            controllerActions._exitState?.TryGetValueOrDefault(state)?.Invoke(data[0], state);
        }

        private static void TransitionState(
            TStateKey from,
            TStateKey to,
            Client client,
            TData[] data,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, TData> controllerActions)
        {
            schemaActions._transition?.Invoke(data, from, to);
            schemaActions._transitionState?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.Invoke(data, from, to);
            controllerActions._transition?.Invoke(data[0], from, to);
            controllerActions._transitionState?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.Invoke(data[0], from, to);
        }

        private static void EnterState(
            TStateKey state,
            Client client,
            TData[] data,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, TData> controllerActions,
            Transitions<TStateKey, TEventKey, TData>.TimeoutTransitions timeoutTransitions,
            TimeoutManager<Client, TStateKey, TData> timeoutRunner)
        {
            schemaActions._enter?.Invoke(data, state);
            schemaActions._enterState?.TryGetValueOrDefault(state)?.Invoke(data, state);
            controllerActions._enter?.Invoke(data[0], state);
            controllerActions._enterState?.TryGetValueOrDefault(state)?.Invoke(data[0], state);

            // end old timeout triggers
            foreach (int id in client.timeouts)
            {
                timeoutRunner.Remove(id);
            }
            client.timeouts.Clear();

            // begin new timeout triggers
            timeoutTransitions.selectors?.Start(timeoutRunner, Time.timeAsDouble, client, client.data, ref client.timeouts);

            // reset flags
            client.state = state;
            client.isStateChangePending = false;
        }

        private static void OnTimeoutComplete(Client client, int timeoutID, bool isStateChangeRequested, TStateKey nextState)
        {
            client.timeouts.Remove(timeoutID);
            if (isStateChangeRequested)
            {
                client.RequestTransition(nextState);
            }
        }

        private static bool TestPolledTransitions(Transitions<TStateKey, TEventKey, TData>.PolledTransitions transitions, TData data, ref TStateKey nextState)
        {
            // test polled selectors
            if (transitions.selectors != null && transitions.selectors.Test(data, ref nextState))
            {
                return true;
            }

            // test polled triggers
            if (transitions.triggers != null)
            {
                foreach (var (to, triggers) in transitions.triggers)
                {
                    if (triggers.Test(data))
                    {
                        nextState = to;
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool TestEventTransitions<TEventContext>(Transitions<TStateKey, TEventKey, TData>.EventTransitions transitions, TData data, TEventContext eventContext, ref TStateKey nextState)
        {
            // test polled selectors
            if (transitions.selectors != null && transitions.selectors.Test(data, eventContext, ref nextState))
            {
                return true;
            }

            // test polled triggers
            if (transitions.triggers != null)
            {
                foreach (var (to, triggers) in transitions.triggers)
                {
                    if (triggers.Test(data, eventContext))
                    {
                        nextState = to;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
