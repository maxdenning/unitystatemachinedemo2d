using System;
using System.Collections.Generic;
using UnityEngine;

namespace Understated
{
    public class MultiClientController<TStateKey, TEventKey, TData> : IController<TStateKey, TEventKey, TData>
        where TStateKey : struct
        where TEventKey : struct
        where TData : class, new()
    {
        [Serializable]
        public class Client : IClient<TStateKey, TData>
        {
            [SerializeField]
            private TData clientData = new TData();

            internal ClientProperties properties = new ClientProperties();

            // IClient interface
            public TData data => clientData;
            public void RequestTransition(TStateKey to) => properties.RequestTransition(to);
        }

        //TODO: does this need to exist? merge with Client class?
        internal class ClientProperties
        {
            //TODO: add actions to clients
            //TODO: instead of iterating, subscribe client side effects to controller/schema side effects
            // public ActionDispatcher<TStateKey, TEventKey, TData> actions = null;

            public List<int> timeouts = new List<int>(0);

            public TStateKey pendingState = default(TStateKey);

            public bool isStateChangePending = false;

            public void RequestTransition(TStateKey to)
            {
                pendingState = to;
                isStateChangePending = true;
            }
        }

        private class ClientTransitionBuffer
        {
            public class Boundary
            {
                public int start = 0;
                public int count = 0;
            }


            public SliceableList<TData> data = new SliceableList<TData>(0);
            public SliceableList<ClientProperties> properties = new SliceableList<ClientProperties>(0);
            public Dictionary<TStateKey, Boundary> boundaries = new Dictionary<TStateKey, Boundary>(0);
            public int Count => properties.Count;


            public void Clear()
            {
                data.Clear();
                properties.Clear();
                boundaries.Clear();
            }

            public (ArraySegment<TData>, ArraySegment<ClientProperties>) Slice(int start, int count)
            {
                return (data.Slice(start, count), properties.Slice(start, count));
            }

            /*
                Assumes self has been cleared and that group.ActiveCount == group.Count
                Outline of algorithm flow:

                    "-" indicates that isStateChangePending = false
                    "_" indicates that the cell is null

                    Find transition chunk sizes for each state:
                        group:  [ A C - B - - A A B - B C ], Count = 12
                        counts: { A: 3, B: 2, C: 2 }
                        buffer: [ ], Count = 0

                    Find transition chunk start indices and allocate buffer:
                        group:  [ A C - B - - A A B - B C], Count = 12
                        starts: { A: 0, B: 3, C: 5 }
                        buffer: [ _ _ _ _ _ _ _ ], Count = 7

                    Copy pending clients from group to sorted buffer:
                        i:      11                        v
                        group:  [ A C - B - - A A B - B | C ], Count = 11
                        counts: { A: 0, B: 0, C: 1 }
                        buffer: [ _ _ _ _ _ C _ ], Count = 7

                        i:      10                      v
                        group:  [ A C - B - - A A B - | B C ], Count = 10
                        counts: { A: 0, B: 1, C: 1 }
                        buffer: [ _ _ _ B _ C _ ], Count = 7

                        i:      8                 v
                        group:  [ A C - B - - A A - | B B C ], Count = 9
                        counts: { A: 0, B: 2, C: 1 }
                        buffer: [ _ _ _ B B C _ ], Count = 7

                        ...

                        i:      0 v
                        group:  [ - - - - | A C B A A B B C ], Count = 4
                        counts: { A: 3, B: 2, C: 2 }
                        buffer: [ A A A B B C C ], Count = 0
                    
                    Clean up garbage memory:
                        group:  [ - - - - | _ _ _ _ _ _ _ _ ], Count = 4
            */
            public bool BufferPendingClients(ClientGroup group)
            {
                // find transition chunk sizes
                bool anyChanges = false;
                for (int i = group.Count - 1; i >= 0; i--)
                {
                    if (group.properties[i].isStateChangePending)
                    {
                        boundaries.TryGetValueOrInitialise(group.properties[i].pendingState).count++;
                        anyChanges = true;
                    }
                }

                if (!anyChanges)
                {
                    return false;
                }

                // find transition chunk start indices
                int bufferSize = 0;
                foreach (var boundary in boundaries.Values)
                {
                    boundary.start = bufferSize;
                    bufferSize += boundary.count;
                    boundary.count = 0;
                }

                // allocate space and set Count as if it were already filled 
                data.SetCount(bufferSize);
                properties.SetCount(bufferSize);

                // copy items to sorted transition buffer and remove from group array
                for (int i = group.Count - 1; i >= 0; i--)
                {
                    if (group.properties[i].isStateChangePending)
                    {
                        Boundary boundary = boundaries[group.properties[i].pendingState];

                        // copy to buffer
                        data[boundary.start + boundary.count] = group.data[i];
                        properties[boundary.start + boundary.count] = group.properties[i];

                        // unsafe remove from group arrays, leaves garbage
                        group.UnsafeRemoveAt(i);

                        // increment count
                        boundary.count++;
                    }
                }

                // clean up group arrays
                group.ClearRange(group.Count, bufferSize);
                group.ActiveCount = group.Count;

                return true;
            }

            // append transition chunks from CurrentState buffer chunks to PendingState groups
            public void CopyToGroups(Dictionary<TStateKey, ClientGroup> clientGroups)
            {
                foreach (var (to, boundary) in boundaries)
                {
                    clientGroups.TryGetValueOrInitialise(to).AddRange(Slice(boundary.start, boundary.count));
                }
            }
        }

        private class ClientGroup
        {
            public SliceableList<TData> data = new SliceableList<TData>(0);
            public SliceableList<ClientProperties> properties = new SliceableList<ClientProperties>(0);
            public int Count => properties.Count;  // total number of clients, active + inactive
            public int ActiveCount = 0;  // number of active clients, where EnterState has been called


            //
            public void Add(TData data, ClientProperties properties)
            {
                this.data.Add(data);
                this.properties.Add(properties);
            }

            // does not confirm slice length is equal
            public void AddRange((ArraySegment<TData>, ArraySegment<ClientProperties>) range)
            {
                this.data.AddRange(range.Item1);
                this.properties.AddRange(range.Item2);
            }

            //TODO: slow
            public bool Remove(Client client)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (properties[i] == client.properties)
                    {
                        data.UnstableRemoveAt(i);
                        properties.UnstableRemoveAt(i);
                        ActiveCount = Count;
                        return true;
                    }
                }
                return false;
            }

            // unsafe remove, leaves garbage
            public void UnsafeRemoveAt(int index)
            {
                data.UnsafeRemoveAt(index);
                properties.UnsafeRemoveAt(index);
            }

            //
            public void Clear()
            {
                data.Clear();
                properties.Clear();
            }

            //
            public void ClearRange(int start, int count)
            {
                data.ClearRange(start, count);
                properties.ClearRange(start, count);
            }

            //
            public (ArraySegment<TData>, ArraySegment<ClientProperties>) Slice(int start, int count)
            {
                return (data.Slice(start, count), properties.Slice(start, count));
            }

            //
            public bool SetCapacity(int capacity)
            {
                return data.SetCapacity(capacity) && properties.SetCapacity(capacity);
            }
        }


        // actions applied to clients controlled by this controller
        public ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> actions { get; private set; } = new ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>>();

        // groups clients by their current state
        private Dictionary<TStateKey, ClientGroup> clientGroups = new Dictionary<TStateKey, ClientGroup>(0);

        // groups clients by their current state
        private Dictionary<TStateKey, ClientTransitionBuffer> transitionBuffers = new Dictionary<TStateKey, ClientTransitionBuffer>(0);

        // marks groups of clientGroups and transitionBuffers for removal
        private List<TStateKey> removeClientGroups = new List<TStateKey>(0);

        // contains newly added clients, cleared after each ProcessStateChanges
        private ClientGroup addedClientGroup = null;
        private ClientTransitionBuffer addedClientBuffer = null;

        // initialised in StartCoroutines()
        private TimeoutManager<ClientProperties, TStateKey, TData> timeoutRunner = null;


        public void AddClient(Client client, TStateKey initialState)
        {
            if (addedClientGroup == null)
            {
                addedClientGroup = new ClientGroup();
                addedClientBuffer = new ClientTransitionBuffer();
            }

            client.properties.pendingState = initialState;
            client.properties.isStateChangePending = true;
            addedClientGroup.Add(client.data, client.properties);
        }

        public void AddClients<TCollection>(TCollection clients, TStateKey initialState)
            where TCollection : ICollection<Client>
        {
            if (addedClientGroup == null)
            {
                addedClientGroup = new ClientGroup();
                addedClientBuffer = new ClientTransitionBuffer();
            }

            addedClientGroup.SetCapacity(addedClientGroup.Count + clients.Count);

            foreach (Client client in clients)
            {
                client.properties.pendingState = initialState;
                client.properties.isStateChangePending = true;
                addedClientGroup.Add(client.data, client.properties);
            }
        }

        //TODO: this is slow
        public bool RemoveClient(Client client)
        {
            foreach (ClientGroup group in clientGroups.Values)
            {
                if (group.Remove(client))
                {
                    return true;
                }
            }
            return false;
        }

        // includes pending clients
        public int Count(TStateKey state)
        {
            return clientGroups.TryGetValueOrDefault(state)?.Count ?? 0;
        }

        // includes pending clients
        public ClientCollection<TData> GetClientData(TStateKey state)
        {
            return clientGroups.TryGetValueOrDefault(state)?.data.AsSlice() ?? new ClientCollection<TData>();
        }

        // includes pending clients
        public void Apply(TStateKey state, Action<ClientCollection<TData>> action)
        {
            ClientGroup group;
            if (clientGroups.TryGetValue(state, out group))
            {
                action(group.data.AsSlice());
            }
        }

        // includes pending clients
        public void Apply(TStateKey state, Action<TData> action)
        {
            ClientGroup group;
            if (clientGroups.TryGetValue(state, out group))
            {
                int count = group.Count;
                for (int i = 0; i < count; i++)
                {
                    action(group.data[i]);
                }
            }
        }

        public void StartCoroutines(MonoBehaviour runner)
        {
            timeoutRunner = new TimeoutManager<ClientProperties, TStateKey, TData>(runner, OnTimeoutComplete);
        }

        public void EndCoroutines(MonoBehaviour runner)
        {
            timeoutRunner.Stop();
            runner.StopCoroutine(timeoutRunner.coroutine);
            //TODO: timeoutRunner = null;
        }

        public void Update(Schema<TStateKey, TEventKey, TData> schema)
        {
            foreach (var (state, group) in clientGroups)
            {
                // schema actions
                schema.actions._update?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);
                
                // controller actions
                this.actions._update?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);
            }
        }

        public void FixedUpdate(Schema<TStateKey, TEventKey, TData> schema)
        {
            foreach (var (state, group) in clientGroups)
            {
                // schema actions
                schema.actions._fixedUpdate?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);

                // controller actions
                this.actions._fixedUpdate?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);
            }
        }

        public void LateUpdate(Schema<TStateKey, TEventKey, TData> schema)
        {
            foreach (var (state, group) in clientGroups)
            {
                // schema actions
                schema.actions._lateUpdate?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);

                // controller actions
                this.actions._lateUpdate?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state);
            }
        }

        //TODO: add overload for sending an event to an individual client?
        public void SendEvent<TEventContext>(Schema<TStateKey, TEventKey, TData> schema, TEventKey eventKey, in TEventContext eventContext)
        {
            foreach (var (state, group) in clientGroups)
            {
                // schema actions
                schema.actions._events?.TryGetValueOrDefault(eventKey)?.Invoke(group.data.AsSlice(), state, eventContext);
                schema.actions._eventsState?.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state, eventContext);

                // controller actions
                this.actions._events?.TryGetValueOrDefault(eventKey)?.Invoke(group.data.AsSlice(), state, eventContext);
                this.actions._eventsState?.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(state)?.Invoke(group.data.AsSlice(), state, eventContext);

                // schema transitions
                if (schema.transitions.HasEventTransitions())
                {
                    TestEventTransitions(schema.transitions.GetEventTransitions(eventKey, state), eventContext, group.data.AsSlice(), group.properties.AsSlice());
                }
            }
        }

        public void PollTransitions(Schema<TStateKey, TEventKey, TData> schema)
        {
            if (schema.transitions.HasPolledTransitions())
            {
                foreach (var (state, group) in clientGroups)
                {
                    TestPolledTransitions(schema.transitions.GetPolledTransitions(state), group.data.AsSlice(), group.properties.AsSlice());
                }
            }
        }

        public void ProcessStateChanges(Schema<TStateKey, TEventKey, TData> schema)
        {
            // check if clients need to change state
            foreach (var (state, buffer) in transitionBuffers)
            {
                if (buffer.BufferPendingClients(clientGroups[state]))
                {
                    //
                    ExitState(state, buffer.data.AsSlice(), schema.actions, this.actions);

                    //
                    TransitionState(state, buffer, schema.actions, this.actions);
                }
            }

            // move pending clients from CurrentState transition buffers to PendingState groups
            foreach (var buffer in transitionBuffers.Values)
            {
                buffer.CopyToGroups(clientGroups);
                buffer.Clear();
            }

            //  move newly added clients from addedClients to correct groups
            if (addedClientBuffer?.BufferPendingClients(addedClientGroup) ?? false)
            {
                addedClientBuffer.CopyToGroups(clientGroups);

                addedClientBuffer.Clear();
                addedClientBuffer = null;

                addedClientGroup.Clear();
                addedClientGroup = null;
            }

            // update client groups
            foreach (var (state, group) in clientGroups)
            {
                // mark empty groups for removal
                if (group.Count <= 0)
                {
                    removeClientGroups.Add(state);
                }
                // enter state for all moved clients, so each client's current state now aligns with pending state
                else if (group.ActiveCount < group.Count)
                {
                    var (data, props) = group.Slice(group.ActiveCount, group.Count - group.ActiveCount);
                    EnterState(state, data, props, schema.actions, this.actions, schema.transitions.GetTimeoutTransitions(state), timeoutRunner);
                    group.ActiveCount = group.Count;
                    transitionBuffers.TryGetValueOrInitialise(state);  // create associated transition buffer if it does not exist
                }
            }

            // remove empty groups
            if (removeClientGroups.Count > 0)
            {
                foreach (TStateKey state in removeClientGroups)
                {
                    clientGroups.Remove(state);
                    transitionBuffers.Remove(state);
                }
                removeClientGroups.Clear();
            }
        }

        private static void TestEventTransitions<TEventContext>(Transitions<TStateKey, TEventKey, TData>.EventTransitions transitions, in TEventContext eventContext, ClientCollection<TData> data, ClientCollection<ClientProperties> properties)
        {
            int count = properties.Count;

            // test event selectors
            if (transitions.selectors != null)
            {
                for (int i = 0; i < count; i++)
                {
                    properties[i].isStateChangePending = properties[i].isStateChangePending || transitions.selectors.Test(data[i], eventContext, ref properties[i].pendingState);
                }
            }

            // test event triggers
            if (transitions.triggers != null)
            {
                foreach (var (to, triggers) in transitions.triggers)
                {
                    for (int i = 0; i < count; i++)
                    {
                        properties[i].isStateChangePending = properties[i].isStateChangePending || triggers.Test(data[i], eventContext);
                        properties[i].pendingState = to;  // only valid if isStateChangePending is true
                    }
                }
            }
        }

        private static void TestPolledTransitions(Transitions<TStateKey, TEventKey, TData>.PolledTransitions transitions, ClientCollection<TData> data, ClientCollection<ClientProperties> properties)
        {
            int count = properties.Count;

            // test polled selectors
            if (transitions.selectors != null)
            {
                for (int i = 0; i < count; i++)
                {
                    properties[i].isStateChangePending = properties[i].isStateChangePending || transitions.selectors.Test(data[i], ref properties[i].pendingState);
                }
            }

            // test polled triggers
            if (transitions.triggers != null)
            {
                foreach (var (to, triggers) in transitions.triggers)
                {
                    for (int i = 0; i < count; i++)
                    {
                        properties[i].isStateChangePending = properties[i].isStateChangePending || triggers.Test(data[i]);
                        properties[i].pendingState = to;  // only valid if isStateChangePending is true
                    }
                }
            }
        }

        private static void ExitState(
            TStateKey state,
            ClientCollection<TData> data,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> controllerActions)
        {
            schemaActions._exit?.Invoke(data, state);
            schemaActions._exitState?.TryGetValueOrDefault(state)?.Invoke(data, state);
            controllerActions._exit?.Invoke(data, state);
            controllerActions._exitState?.TryGetValueOrDefault(state)?.Invoke(data, state);
        }

        private static void TransitionState(
            TStateKey from,
            ClientTransitionBuffer buffer,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> controllerActions)
        {
            foreach (var (to, boundary) in buffer.boundaries)
            {
                var data = buffer.data.Slice(boundary.start, boundary.count);
                schemaActions._transition?.Invoke(data, from, to);
                schemaActions._transitionState?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.Invoke(data, from, to);
                controllerActions._transition?.Invoke(data, from, to);
                controllerActions._transitionState?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.Invoke(data, from, to);
            }
        }

        private static void EnterState(
            TStateKey state,
            ClientCollection<TData> data,
            ClientCollection<ClientProperties> properties,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> schemaActions,
            ActionDispatcher<TStateKey, TEventKey, ClientCollection<TData>> controllerActions,
            Transitions<TStateKey, TEventKey, TData>.TimeoutTransitions timeoutTransitions,
            TimeoutManager<ClientProperties, TStateKey, TData> timeoutRunner)
        {
            // size of client list is not modified so cache 
            int count = data.Count;

            // exit events
            schemaActions._enter?.Invoke(data, state);
            schemaActions._enterState?.TryGetValueOrDefault(state)?.Invoke(data, state);
            controllerActions._enter?.Invoke(data, state);
            controllerActions._enterState?.TryGetValueOrDefault(state)?.Invoke(data, state);

            for (int i = 0; i < count; i++)
            {
                var props = properties[i];

                // end old timeout triggers
                if (props.timeouts.Count > 0)
                {
                    for (int j = 0; j < props.timeouts.Count; j++)
                    {
                        timeoutRunner.Remove(props.timeouts[j]);
                    }
                    props.timeouts.Clear();
                }

                // reset flags
                props.isStateChangePending = false;
            }

            // begin new timeout triggers
            double timeNow = Time.timeAsDouble;
            if (timeoutTransitions.selectors != null)
            {
                for (int i = 0; i < count; i++)
                {
                    timeoutTransitions.selectors.Start(timeoutRunner, timeNow, properties[i], data[i], ref properties[i].timeouts);
                }
            }
        }

        private static void OnTimeoutComplete(ClientProperties props, int timeoutID, bool isStateChangeRequested, TStateKey nextState)
        {
            props.timeouts.Remove(timeoutID);
            if (isStateChangeRequested)
            {
                props.RequestTransition(nextState);
            }
        }
    }
}
