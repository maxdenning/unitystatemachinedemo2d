using UnityEngine;

namespace GettingStarted
{
    public class InputHandler : MonoBehaviour
    {
        // use the generic type for all controller MonoBehaviours
        [SerializeField]
        private Understated.ControllerComponent<MyStates, MyEvents, MyData> controller;

        void Update()
        {
            // emit the event whenever space is pressed
            if (Input.GetKeyDown("space"))
            {
                Color randomColour = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                controller.SendEvent(MyEvents.ChangeColour, randomColour);
            }
        }
    }
}
