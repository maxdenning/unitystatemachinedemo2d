using System;
using UnityEngine;

namespace Platformer2D
{
    public class PlayerWallSlideTriggers : MonoBehaviour
    {
        [Serializable]
        public struct TriggerPosition
        {
            public Vector2 offset;
            public Vector2 size;
        }

        public bool isWallLeft { get => leftCollider != null; }
        public bool isWallRight { get => rightCollider != null; }

        public Collider2D leftCollider { get; private set; } = null;
        public Collider2D rightCollider { get; private set; } = null;

        [SerializeField]
        private TriggerPosition leftTrigger;

        [SerializeField]
        private TriggerPosition rightTrigger;

        //DEBUG
        private void OnDrawGizmos()
        {
            Vector3 scale = Abs(transform.localScale);
            Vector3 position = transform.position;

            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(position + Vector3.Scale(leftTrigger.offset, scale), Vector3.Scale(leftTrigger.size, scale));
            
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(position + Vector3.Scale(rightTrigger.offset, scale), Vector3.Scale(rightTrigger.size, scale));
        }

        private void FixedUpdate()
        {
            // check collisions - abosulte direction, not affected by flipping transform
            Vector3 scale = Abs(transform.localScale);
            Vector3 position = transform.position;
            int layerMask = ~(1 << gameObject.layer);
            leftCollider = Physics2D.OverlapCapsule(position + Vector3.Scale(leftTrigger.offset, scale), Vector3.Scale(leftTrigger.size, scale), CapsuleDirection2D.Vertical, 0f, layerMask);
            rightCollider = Physics2D.OverlapCapsule(position + Vector3.Scale(rightTrigger.offset, scale), Vector3.Scale(rightTrigger.size, scale), CapsuleDirection2D.Vertical, 0f, layerMask);
        }

        private static Vector3 Abs(Vector3 source)
        {
            return new Vector3(
                Mathf.Abs(source.x),
                Mathf.Abs(source.y),
                Mathf.Abs(source.z)
            );
        }
    }
}
