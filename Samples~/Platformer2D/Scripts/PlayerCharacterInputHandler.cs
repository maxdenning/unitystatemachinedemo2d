using UnityEngine;
using UnityEngine.InputSystem;

namespace Platformer2D
{
    public class PlayerCharacterInputHandler : MonoBehaviour
    {
        [SerializeField]
        private PlayerMovementController controller;

        public void OnMove(InputAction.CallbackContext context)
        {
            controller.client.data.moveDirection = context.ReadValue<Vector2>();
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Performed)
            {
                controller.SendEvent<InputAction.CallbackContext>(PlayerMovementSchema.Events.AttackInput, context);
            }
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            switch (context.phase)
            {
                case InputActionPhase.Performed:
                    controller.SendEvent<InputAction.CallbackContext>(PlayerMovementSchema.Events.JumpInputBegin, context);
                    break;
                
                case InputActionPhase.Canceled:
                    controller.SendEvent<InputAction.CallbackContext>(PlayerMovementSchema.Events.JumpInputEnd, context);
                    break;
            }
        }
    }
}
